$(document).ready(function(){
    $('#cartBackdropPhoneModal').click(() => {
        $('#cartInputPhone').fadeOut();
        toggleScrolling(false)
    })

    $('.cart__items').click((ev) => {
        let elem;
        if ($(ev.target).is('#cart__minus')) {
            elem = $(ev.target).next();
            let q = $(elem).text();
            if (parseInt(q) > 1) {
                q = parseInt(q) - 1;
                $(elem).text(q);
            }
        } else if ($(ev.target).is('#cart__plus')) {
            elem = $(ev.target).prev();
            let q = $(elem).text();
            q = parseInt(q) + 1;
            $(elem).text(q);
        }
    })
});
function cartShowPhoneModal() {
    $('#cartInputPhone').fadeIn();
    toggleScrolling(true)
}
function cartClosePhoneModal() {
    $('#cartInputPhone').fadeOut();
    toggleScrolling(false)
}  
function toggleScrolling(lock) {
    if (lock) {
        $("body").css("overflow", "hidden");
    } else {
        $("body").css("overflow", "");
    }
}
function changeCount(increment, id) {
    // let val = $('#cart__item-count_' + id).text();
    // if (increment) {
    //     val = parseInt(val) + 1;
    // } else {
    //     if (parseInt(val) > 1) {
    //         val = parseInt(val) - 1;
    //     }
    // }
    // $('#cart__item-count_' + id).text(val);
}

