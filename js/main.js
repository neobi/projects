$(document).ready(function(){
  $('.slider').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 500,
    autoplay: false, 
    centerMode: true,
    responsive: [
        {
          breakpoint: 996,
          settings: {
            slidesToShow: 1.2,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        }
    ]
  });
  $('#backdropGoodModal').click(() => {
      $('#chooseGood').fadeOut();
      selectDefaultGoodModal();
      toggleScrolling(false)
  })
});

//Good modal main
function showGoodModal() {
    $('#chooseGood').fadeIn();
    toggleScrolling(true)
}
function closeGoodModal() {
    $('#chooseGood').fadeOut();
    toggleScrolling(false)
    selectDefaultGoodModal();
}
function selectDefaultGoodModal() {
    $("#good_type_image").attr("src",'images/small_pizza.png');
    $('#good__first').addClass('choosenGoodType');
    removeOldSelection('good__first');
}
function selectGoodType(id) {
    let hId = '';
    if (id == 1) {
        $('#good__first').addClass('choosenGoodType');
        $('#good_type_image').attr('src', 'images/small_pizza.png');
        $('.good__modal-about-size').text('25 см, 410 г')
        $('.good__modal-about-text').text('Цыпленок, ветчина, пикантная пепперони, острая чоризо, моцарелла, томатный соус')
        hId = 'good__first';
    } else if (id == 2) {
        $('#good__second').addClass('choosenGoodType');
        $('#good_type_image').attr('src', 'images/medium_pizza.png');
        $('.good__modal-about-size').text('30 см, 600 г')
        $('.good__modal-about-text').text('Цыпленок, ветчина, пикантная пепперони, острая чоризо, моцарелла, томатный соус')

        // $('#good_type_image').fadeOut(300, function() {
        //     $('#good_type_image').attr('src', 'images/medium_pizza.png');
        // }).fadeIn(300);
        // $("#good_type_image").fadeTo(1000, 0.3, function() {
        //     $("#good_type_image").attr("src",'images/medium_pizza.png');
        //     $("#good_type_image").on('load', function(){
        //       $("#good_type_image").fadeTo(300,1);
        //     });
        //    });
        hId = 'good__second';
    } else {
        $('.good__modal-about-size').text('35 см, 800 г')
        $('.good__modal-about-text').text('Цыпленок, ветчина, пикантная пепперони, острая чоризо, моцарелла, томатный соус')
        $('#good__third').addClass('choosenGoodType');
        $("#good_type_image").attr("src",'images/big_pizza.png');
        hId = 'good__third';
    }
    removeOldSelection(hId);
}
function removeOldSelection(hid) {
    $('.good__modal-types').children().each(function(i, el) {
        if ($(el).hasClass('choosenGoodType') && !$(el).is('#' + hid)) {
            $(el).removeClass('choosenGoodType')
        }
    })
}

function toggleScrolling(lock) {
    if (lock) {
        $("body").css("overflow", "hidden");
    } else {
        $("body").css("overflow", "");
    }
}


// Phone modal main 


